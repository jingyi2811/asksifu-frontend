import { BoxProps } from '@asksifu/uikit'

export interface PageHeaderProps extends BoxProps {
  background?: string
}
