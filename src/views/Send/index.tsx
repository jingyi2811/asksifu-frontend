import styled from 'styled-components'
import useActiveWeb3React from '../../hooks/useActiveWeb3React'

const Div = styled.div`
  padding: 8px;
  font-family: Helvetica;
  letter-spacing: 2px;
  line-height: 2;
  @media (max-width: 900px) {
    padding: 5px;
    letter-spacing: 1px;
    line-height: 1.5;
  }
`

function Send() {
  const { account } = useActiveWeb3React()

  // if (!account) {
  //   return (
  //    <>
  //      <Div>
  //        Remember to connect wallet to proceed.
  //      </Div>
  //    </>
  //   )
  // }

  return (
   <>Still under development</>
  )
}

export default Send
